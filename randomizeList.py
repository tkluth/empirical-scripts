#!/usr/bin/python
# -*- coding: utf8 -*-

# Copyright(c) 2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
#
# Since October 2014, the development of this software was supported by the
# Excellence Cluster EXC 277 Cognitive Interaction Technology.
# The Excellence Cluster EXC 277 is a grant of the Deutsche
# Forschungsgemeinschaft (DFG) in the context of the German
# Excellence Initiative.

import numpy

from numpy import random,loadtxt,savetxt

# name of the list to be randomized
fileNameProtoList = "protolist.csv"
# how many practice trials are at the beginning of the file
practiceTrials = 2
# of how many columns does one row (i.e., one row) consist?
noOfColumns = 16
# in what column number is the information stored whether this trial is a critical (value "TRUE") or a filler item (value "FALSE")
criticalColumn = 2
# amount of trials/items (i.e., rows in the table) incl. practice trials
noOfTrials = 14
# how many random lists should be created
numberOfLists = 10

# no need to change below this line (if you are happy with the only constraint that two subsequent items should not be critical)

randomizedList = numpy.empty([noOfTrials,], dtype=object)

for listNo in range(numberOfLists) :
	randomizedList = loadtxt(fileNameProtoList, dtype=str, delimiter='\t', skiprows=1)

	# the following only loads rows practiceTrials to the end (ignoring practice trials which should not be randomized but always be in the beginning) and columns 0 to noOfColumns
	randomizedList = randomizedList[practiceTrials:, 0:noOfColumns]

	# randomize
	random.shuffle(randomizedList)

	# check constraints; swap rows if necessary
	constraintViolations = 1 # greater than zero to enter loop


	while constraintViolations > 0 :
		for row in range(len(randomizedList) - 1) :
			if(row < 2): # only in the first two rows
				if(randomizedList[row, criticalColumn] == '"TRUE"'):
					# critical items among the first two
					constraintViolations = constraintViolations + 1
					randomRow = random.random_integers(low = 2, high = len(randomizedList) - 1) # swap the critical item with a different one (but not with the first two rows)
					tmpRow = numpy.copy(randomizedList[row])
					randomizedList[row] = randomizedList[randomRow]
					randomizedList[randomRow] = tmpRow
			if ((randomizedList[row, criticalColumn] == '"TRUE"') & (randomizedList[row, criticalColumn] == randomizedList[row + 1, criticalColumn])) :
				# two critical items in a row
				constraintViolations = constraintViolations + 1
				randomRow = random.random_integers(low = 2 ,high = len(randomizedList) - 1) # swap the first occurence with random row (but not with the first two rows)
				tmpRow = numpy.copy(randomizedList[row])
				randomizedList[row] = randomizedList[randomRow]
				randomizedList[randomRow] = tmpRow

		# print some information
		print 'randomizing list ' + str(listNo)
		# print 'number of violations before swapping '
		# print constraintViolations

		constraintViolations = 0

		for row in range(len(randomizedList) - 1) :
			if(row < 2): # only in the first two rows
				if(randomizedList[row, criticalColumn] == '"TRUE"'):
					 # critical items among the first two
					constraintViolations = constraintViolations + 1
			if ((randomizedList[row, criticalColumn] == '"TRUE"') & (randomizedList[row, criticalColumn] == randomizedList[row + 1, criticalColumn])) :
				# two critical items in a row
				constraintViolations = constraintViolations + 1

		# print some information
		# print 'number of violations after swapping'
		# print constraintViolations

	# add the practice trials in the front
	completeList = loadtxt(fileNameProtoList, dtype=str, delimiter='\t', skiprows=1)[:,0:noOfColumns]
	completeList[practiceTrials:,:] = randomizedList

	savetxt('randomList'+str(listNo)+'.csv', completeList, fmt='%s', delimiter='\t')

