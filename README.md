This is a collection of scripts I wrote for running my empirical eye tracking study, compatible with SR Research EyeLink 1000 and SR Research Experiment Builder and Data Viewer.
One script (written in `python`) (pseudo-)randomizes stimuli lists, the other script (written in `R`) generates log-gaze plots from a fixation report file (as saved by SR Research Data Viewer) and applies a sample Bayesian regression analysis using [brms](https://cran.r-project.org/web/packages/brms/index.html). Especially the second script serves as an *example* script that needs to be adapted to fit to your data and study (empirical sample data for which the script was written is provided). I hope that the script is helpful for conducting an analysis of eye-tracking data using `R` -- despite the needed manual adaptions.

# License 

All source code is licensed under the [GNU General Public License v3.0](https://gitlab.com/tkluth/empirical-scripts/blob/master/LICENSE).

# List Randomizer

The [`randomizeList.py`](https://gitlab.com/tkluth/empirical-scripts/blob/master/randomizeList.py) python script creates pseudo-randomized copies of a list where each row denotes an experimental trial.
Currently, it ensures the following two constraints (hence pseudo-randomization):

1. among the first two items should be no critical item
2. after a critical item should appear a non-critical item (no two critical items directly following each other)

For more / different constraints, you need to change _all_ `if` statements in the script accordingly.

The input for the script is a list as `.csv` file with the following specifications (see protolist.csv for an example list):
- the list needs a boolean column that states whether the row is critical or not; this column should contain only two values (as strings): "TRUE" or "FALSE"
- tab-delimited
- quote strings with `"`

You probably want to adjust the 6 variables at the top of [`randomizeList.py`](https://gitlab.com/tkluth/empirical-scripts/blob/master/randomizeList.py) and then start the script with

```
python randomizeList.py
```

If successful, the script saves files named `randomListX.csv` (where `X` is a unique number) that can be used as lists for deployed Experiment Builder experiments.

# Log Gaze Plots from Fixation Reports

The file [`plots_and_analysis.R`](https://gitlab.com/tkluth/empirical-scripts/blob/master/plots_and_analysis.R) was used as teaching material in a course on psycholinguistical eye-tracking. The file [`fixation_report_no_preview.txt`](https://gitlab.com/tkluth/empirical-scripts/blob/master/fixation_report_no_preview.txt) contains the collected eye tracking data (as stored by Data Viewer). The script contains several occurrences of variables and factors investigated in the specific study, especially in the beginning: Column names `prosody, condition, item, critical, balancing, list, image_file, sound_file, picP` and several columns named `np1on, np1off` etc. The `on` and `off` column names denote on- and offsets of the corresponding words. Obviously, you will need to adjust these lines to the logic of your study.
Apart from that, the script is ready to go. Just execute one line after the other, read the comments to understand what's going on and you will soon see one ratio-of-proportions and one log-gaze plot.

# Bayesian Analysis of Eye Tracking Data from Fixation Reports

The second part of the file [`plots_and_analysis.R`](https://gitlab.com/tkluth/empirical-scripts/blob/master/plots_and_analysis.R) provides the source code for an example of a Bayesian regression analysis (with different models and a comparison to an NHST analysis) of the provided empirical data using the [R package brms](https://cran.r-project.org/web/packages/brms/index.html). Again, if you execute the code line by line and read the comments, you should see the outcome of this statistical analysis. Of course you will need to adapt the specific statistical models to your data and research question.
